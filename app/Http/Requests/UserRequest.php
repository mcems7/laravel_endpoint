<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'id' => 'required|integer|unique:users',
            'name'=>'required|string|max:255',
            'last_name'=>'required|string|max:255',
            'role' => 'required',Rule::in(['admin','doctor','paciente']),
            //'address'=>'string',
            'phone'=>'integer',
            'bornday'=>'string',
            //'description'=>'string',
            'email' => 'required|unique:users,id,'.$this->route('id'),
            //'password' => 'required|string|min:6|confirmed',
        ];
    }

    public function messages()
    {
        return [
            //'id.required'  => 'La Identificación es obligatoria',
            //'id.integer' => 'La Identificación debe ser numérico.',
            //'id.unique' => 'La Identificación ya está registrada.',
            
            'name.required'  => 'El nombre es obligatorio',
            'name.string'  => 'El nombre debe ser un texto',
            'name.max' => 'El nombre no puede tener más de 255 caracteres',
            
            
            'last_name.required'  => 'El apellido es obligatorio',
            'last_name.string'  => 'El apellido debe ser un texto',
            'last_name.max' => 'El apellido no puede tener más de 255 caracteres',
            
            'role.required'  => 'El rol es obligatorio',
            'role.in'  => 'El rol debe ser válido',
            
            'address.string'  => 'El nombre debe ser un texto',

            'phone.integer'  => 'El teléfono débe ser de tipo número',

            'bornday.date'  => 'el formato de la fecha de nacimiento no es correcto',

            'description.string'  => 'La descripción debe ser un texto',
            
            'email.required'  => 'El correo electrónico es obligatorio',
            'email.email'  => 'El correo electrónico debe tener el formato válido como ejemplo@dominio.com o similar',
            'email.max'  => 'El correo electrónico no puede tener más de 255 caracteres',
            'email.unique'  => 'El correo electrónico ya está en uso',
            
            'password.required'  => 'La contraseña es obligatorio',
            'password.string'  => 'La contraseña debe ser un texto',
            'password.max' => 'La contraseña no puede tener más de 255 caracteres',
            'password.confirmed'  => 'La confirmación de la contraseña no coincide',
            
        ];
    }
}
