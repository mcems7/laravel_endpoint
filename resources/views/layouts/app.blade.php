<?php
$periodo_activo = "";//Funciones::periodo_activo();
?><!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- -->
    @if (env('EDIV_LIMPIAR_CACHE')==true)
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" /> 
    @endif
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Ediv') }}</title>

    <!-- Styles -->
    <link href="{{ asset('lib/bootstrap-3.3.7-dist/css/bootstrap.min.css', env('HTTPS')) }}" rel="stylesheet" />
    <link href="{{ asset('lib/bootstrap-switch/bootstrap-switch.css', env('HTTPS')) }}" rel="stylesheet" />
    <link href="{{ asset('css/app.css', env('HTTPS')) }}" rel="stylesheet">
    <link href="{{ asset('fonts/fontawesome/css/all.css', env('HTTPS')) }}" rel="stylesheet"> <!--load all styles -->
    
    <link href="{{ asset('css/ediv.css', env('HTTPS')) }}" rel="stylesheet">
    <link href="{{ asset('css/calendario.css', env('HTTPS')) }}" rel="stylesheet">
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <style>
      .btn.lista {
           width: 85.5px !important;
            margin: 2px 0px !important;
      }
    </style>
    <!--amcharts-->
<script src="{{ asset('lib/amcharts/core.js', env('HTTPS')) }}"></script>
<script src="{{ asset('lib/amcharts/charts.js', env('HTTPS')) }}"></script>
<script src="{{ asset('lib/amcharts/themes/material.js', env('HTTPS')) }}"></script>
<script src="{{ asset('lib/amcharts/themes/animated.js', env('HTTPS')) }}"></script>
<script src="{{ asset('lib/amcharts/lang/de_DE.js', env('HTTPS')) }}"></script>
<!--amcharts-->
@yield('style')
</head>
<body>
    <div id="app" style="min-height: 550px;">
        @include('layouts.partials.nav')
        <div id="alerts" style="position:relative;z-index:999;width:100%;top:0px;">
        @if (session('notice'))
        <div class="alert alert-info alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session('notice') }}
        </div>
        @endif
        @if (session('success'))
        <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session('success') }}
        </div>
        @endif
        @if (session('warning'))
        <div class="alert alert-warning alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session('warning') }}
        </div>
        @endif
        @if (session('danger'))
        <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session('danger') }}
        </div>
        @endif
        
        
        
        </div>
        @hasSection ('content')
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">

                  
                  <span class="hidden-print">
                    @yield('ruta_de_migas')
                  </span>
                  <span class="hidden-print">
                    @yield('barra_buscar_cuerpo')
                  </span>
                    @yield('content')
                </div>
            </div>
        </div>
        @endif
    </div>
<footer style="width:95%">

  </footer>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js', env('HTTPS')) }}"></script>
    <script src="{{ asset('lib//bootstrap-switch/bootstrap-switch.js', env('HTTPS')) }}"></script>
    <script src="{{ asset('lib//bootstrap-switch/highlight.js', env('HTTPS')) }}"></script>
    <script src="{{ asset('lib//bootstrap-switch/main.js', env('HTTPS')) }}"></script>
    <script src="{{ asset('lib/select2/select2.min.js', env('HTTPS')) }}"></script>
    <link href="{{ asset('lib/select2/select2.min.css', env('HTTPS')) }}" rel="stylesheet" />
    
   
<script>
    // In your Javascript (external .js resource or <script> tag)
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });
    $(".js-example-tokenizer").select2({
    tags: true,
    tokenSeparators: [', ', ' ']
})
</script>
<script src="{{ asset('lib/stopExecutionOnTimeout/stopExecutionOnTimeout.js', env('HTTPS')) }} "></script>

<!--
<script src="https://static.codepen.io/assets/common/stopExecutionOnTimeout-de7e2ef6bfefd24b79a3f68b414b87b8db5b08439cac3f1012092b2290c719cd.js"></script>
-->
<script>
    // COPY TO CLIPBOARD
// Attempts to use .execCommand('copy') on a created text field
// Falls back to a selectable alert if not supported
// Attempts to display status in Bootstrap tooltip
// ------------------------------------------------------------------------------

function copyToClipboard(text, el) {
  var copyTest = document.queryCommandSupported('copy');
  var elOriginalText = el.attr('data-original-title');

  if (copyTest === true) {
    var copyTextArea = document.createElement("textarea");
    copyTextArea.value = text;
    document.body.appendChild(copyTextArea);
    copyTextArea.select();
    try {
      var successful = document.execCommand('copy');
      var msg = successful ? text+': Copiado a portapapeles!' : 'Upss, no se ha copiado!';
      el.attr('data-original-title', msg).tooltip('show');
    } catch (err) {
      console.log('Upss, esta desactivada la función ´copy´');
    }
    document.body.removeChild(copyTextArea);
    el.attr('data-original-title', elOriginalText);
  } else {
    // Fallback if browser doesn't support .execCommand('copy')
    window.prompt("Para copiar al portapapeles utilice: Ctrl+C o Command+C", text);
  }
}

$(document).ready(function() {
  // Initialize
  // ---------------------------------------------------------------------

  // Tooltips
  // Requires Bootstrap 3 for functionality
  $('.js-tooltip').tooltip();

  // Copy to clipboard
  // Grab any text in the attribute 'data-copy' and pass it to the 
  // copy function
  $('.js-copy').click(function() {
    var text = $(this).attr('data-copy');
    var el = $(this);
    copyToClipboard(text, el);
  });
});
  
$(document).ready(function() {
  //atajos de teclado
$(document).bind('keydown', function(e) {
tecla=(document.all) ? e.keyCode : e.which;
if (tecla==65 && e.shiftKey && e.altKey){ //Shift + Alt + Tecla A => admin
//acciones
if (tecla==68 && e.shiftKey && e.altKey){ //Shift + Alt + Tecla D => director
//acciones
if (tecla==84 && e.shiftKey && e.altKey){ //Shift + Alt + Tecla T => docente
//acciones
if (tecla==69 && e.shiftKey && e.altKey){ //Shift + Alt + Tecla E => estudiante
//acciones
if (tecla==80 && e.ctrlKey){return false;}//Control + Tecla P => nada
});//fin bind('keydown')
  
$("#roles[data-toggle=popover]").popover({
    html: true, 
	content: function() {
          return $('#popover-roles').html();
        }
});
});//fin ready
</script>
    @yield('scripts')
<script>
  $(document).ready(function() {
  $("#formulario #origen>option").css("white-space",'pre-wrap');
  $("#formulario #destino>option").css("white-space",'pre-wrap');
    
  $("#formulario #origen").css("min-width",'100%');
  $("#formulario #origen").css("height",'auto');
  $("#formulario #origen").css("height",'-webkit-fill-available');
    
  $("#formulario #destino").css("min-width",'100%');
  $("#formulario #destino").css("height",'auto');
  $("#formulario #destino").css("height",'-webkit-fill-available');
 });
</script>
</body>
</html>
