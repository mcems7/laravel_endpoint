<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\User;
use App\Functions;
use Redirect;
use Hash;
use Storage;
use Auth;
class UserController extends Controller {

public $user;
public $palabra_clave_buscar = '';
public $excepciones_buscar = ['password'];
public $dato = '';//aux
public $resultados = 8;

   public function __construct()
    {
    }
  public function api_photo($filename){
    $exists = Storage::disk('users')->exists($filename); 
      if ($exists){
        $url = Storage::disk('users')->url($filename); 
        return response()->json(['photo' => $url], 200);
      }else{
          $user = User::find($filename);
          if ($user){
          $exists = Storage::disk('users')->exists($user->photo);
          if ($exists){
            $url = Storage::disk('users')->url($user->photo);
            $user->photo=$url;
           }else{
            $user->photo=env('APP_URL').'/user.png';
           }
          return response()->json($user);
          }else{
          return response()->json(['ok' => 'false'], 404);  
          }
      }
    }
    public function api_users(Request $request){
      $user = new User();
      $resp = $user->paginate($this->resultados);
      foreach ($resp as $resp_i){
      $exists = Storage::disk('users')->exists($resp_i->photo); 
        if ($exists){
          $url = Storage::disk('users')->url($resp_i->photo);
          $resp_i->photo=$url;
        }else{
          $resp_i->photo=env('APP_URL').'/user.png';
        }
      }
      return response()->json($resp);
    }
}
