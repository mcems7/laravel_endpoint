<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if (Auth::check()){
            $roles = explode("|",$role);
            if (!in_array(Auth::user()->role, $roles)) {//admin|doctor|paciente
                return redirect('home');
            }
        }
    return $next($request);
    }
}
